import ETH from "./modules/eth.js";
import UI from "./modules/ui.js";

class Votation {
  contractInstance;

  // Initialisation de la votation
  async init () {
    // Recuperation of ethereum related info thanks to web3
    let web3 = await ETH.initAndGetWeb3();
    if (!web3) {
      alert("Please install an Ethereum-compatible browser or extension like MetaMask to use this dApp!");
      return;
    }

    // loading current metamask account
    let accountAddress = await web3.eth.getCoinbase();
    // loading current metamask network
    let networkId = await web3.eth.net.getId();

    // loading Election Smart Contract information saved after compilation
    let electionContractInfo = await this.getElectionContractInfo();

    // start polling in order to be sure that metamask account, networkId and contract info have not changed
    setInterval(async function() {
      ETH.haveWeb3InfoChanged(web3, accountAddress, networkId, electionContractInfo, this.getElectionContractInfo).then(haveChanged => {
        // if it has change => reload the page
        if (haveChanged) {
          document.location.reload();
        }
      });
    }.bind(this), 5000);

    // Let's show informations on the user interface
    UI.showEthInfo(web3, accountAddress, networkId, electionContractInfo);

    // now let's read the smart contract on the block chain
    // 1. find the address of the smart contract on the blockchain
    const contractAddress = ETH.getContractAddressFromInfo(electionContractInfo, networkId);
    // 2. get the instance of the smart contract
    this.contractInstance = await new web3.eth.Contract(electionContractInfo.abi, contractAddress);

    // We are all set, let the people vote
    // 1. On récupère les résultats en cours
    this.reloadVoteResults();

    // 2. on écoute le contrat en cours pour savoir si un évènement "newvoteRegistered" est lancé
    this.contractInstance.events.newVoteRegistered().on("data", function() {
      // si cette évènement est lancé, on récupère les nouveaux résultats
      this.reloadVoteResults();
    }.bind(this));

    // 3. on écoute le contrat en cours pour savoir si un évènement "reset" est lancé
    this.contractInstance.events.reset().on("data", function() {
      // si cette évènement est lancé, on récupère les nouveaux résultats
      this.reloadVoteResults();
    }.bind(this));

    // On branche les boutons de vote
    document.getElementById("voteForA").onclick = () => {
      this.vote("A", accountAddress);
    };
    document.getElementById("voteForB").onclick = () => {
      this.vote("B", accountAddress);
    };
    document.getElementById("resetElection").onclick = () => {
      this.resetElection(accountAddress);
    };
  }

  /**
   * Retourne les informations du smart contract des éléctions (le résultat de la compulation truffle)
   * @return {object} truffle compilation info
   */
  getElectionContractInfo () {
    return new Promise((resolve) => {
      fetch("./build/Election.json?t=" + Date.now())
        .then(response => {
          resolve(response.json());
        });
    });
  }

  /**
   * Lis les résultats de la votation en cours
   */
  async reloadVoteResults () {
    try {
      const aScore = await this.contractInstance.methods.aScore().call();
      const bScore = await this.contractInstance.methods.bScore().call();
      const counter = await this.contractInstance.methods.counter().call();
      document.getElementById("aScore").textContent = aScore || 0;
      document.getElementById("bScore").textContent = bScore || 0;
      document.getElementById("counter").textContent = counter || 0;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  /**
   * Enregistre un vote sur la blockchain
   * @param  {string}        choice           Pour qui votez vous ?
   * @param  {address}       accountAddress   L'utilisateur votant
   */
  vote (choice, accountAddress) {
    let waitingForConfirmation = false;
    let methodId = null;

    if (choice === "A") {
      methodId = "voteForA";
    } else if (choice === "B") {
      methodId = "voteForB";
    } else {
      return false;
    }

    return this.contractInstance.methods[methodId]().send({
      from: accountAddress
    }).on("transactionHash", () => {
      waitingForConfirmation = true;
    }).on("confirmation", () => {
      if (waitingForConfirmation) {
        waitingForConfirmation = false;
      }
    }).on("error", (err) => {
      waitingForConfirmation = false;
      throw err;
    });
  }

  /**
   * reset un vote sur la blockchain
   * @param  {address}       accountAddress   L'utilisateur votant
   */
  resetElection (accountAddress) {
    let waitingForConfirmation = false;
    let methodId = null;

    return this.contractInstance.methods['resetElection']().send({
      from: accountAddress
    }).on("transactionHash", () => {
      waitingForConfirmation = true;
    }).on("confirmation", () => {
      if (waitingForConfirmation) {
        waitingForConfirmation = false;
      }
    }).on("error", (err) => {
      waitingForConfirmation = false;
      throw err;
    });
  }
}

new Votation().init();


/**
 * Récupère l'adresse d'un contract sur un network donné en parsant ses infos
 * @param  {Object}         contractInfo Les informations du contrat (provient de la compilation de Truffle)
 * @param  {int} networkId  L'identifiant du réseau (Ropsten, Ganache...)
 * @return {adress|false}   Retourne l'adresse du contract sur le réseau donné, si pas d'adresse, retourne False
 */
const getContractAddressFromInfo = function (contractInfo, networkId) {
  if (contractInfo.networks && contractInfo.networks[networkId] && contractInfo.networks[networkId].address) {
    return contractInfo.networks[networkId].address;
  } else {
    return false;
  }
};

export default {

  /**
   * Fonction initialisant metamask
   * @return instance web3
   */
  initAndGetWeb3: async function () {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      try {
        await window.ethereum.enable();
      } catch (e) {
        return false;
      }

      return window.web3;
    }
    return false;
  },

  /**
   * voir plus haut dans le fichier
   */
  getContractAddressFromInfo: getContractAddressFromInfo,

  /**
   * Est-ce que les diverses informations web3 ont changé ? Le compte sur la blockchain, la blockchain que l'on inspecte, les infos truffle du contract à lire ont changé ?
   * @param  {web3 instance}  web3                L'instance Web3 à inspecter pour obtenir de nouvelles  informations
   * @param  {address}        accountAddress       L'adresse du compte que l'on compare
   * @param  {int}            networkId            l'id du network que l'on compare
   * @param  {Object}         contractInfo         les infos du contrat que l'on compare
   * @param  {async Function} loadNewContractInfo  Fonction permettant de récupérer les nouvelles informations du contrats
   * @return {Boolean}                             oui ou non
   */
  haveWeb3InfoChanged: async function (web3, accountAddress, networkId, contractInfo, loadNewContractInfo) {

    const justLoadedAccountAddress = await web3.eth.getCoinbase();
    const justLoadedNetworkId = await web3.eth.net.getId();
    return new Promise(resolve => {

      if (justLoadedAccountAddress !== accountAddress) {
        return resolve(true);
      }

      if (justLoadedNetworkId !== networkId) {
        return resolve(true);
      }

      loadNewContractInfo().then(justLoadedElectionContractInfo => {
        if (getContractAddressFromInfo(contractInfo, networkId) !== getContractAddressFromInfo(justLoadedElectionContractInfo, networkId)) {
          return resolve(true);
        }

        return resolve(false);
      });
    });
  }

};

import ETH from "./eth.js";

export default {

  showEthInfo (web3, accountAddress, networkId, electionContractInfo) {
    document.getElementById("accountAddress").textContent = accountAddress ? accountAddress : "not found";
    document.getElementById("metamaskConnected").textContent = web3 ? "connected" : "not connected";

    let contractAddress = ETH.getContractAddressFromInfo(electionContractInfo, networkId);
    if (contractAddress) {
      document.getElementById("contractAddress").textContent = contractAddress;
    } else {
      document.getElementById("contractAddress").textContent = "is not loaded";
    }

    switch (networkId) {
    case 1:
    case 1n:
      document.getElementById("metmaskNetworkName").textContent = "Main Net ";
      break;
    case 2:
    case 2n:
      document.getElementById("metmaskNetworkName").textContent = "Deprecated Morden test network ";
      break;
    case 3:
    case 3n:
      document.getElementById("metmaskNetworkName").textContent = "Ropsten test network ";
      break;
    case 4:
    case 4n:
      document.getElementById("metmaskNetworkName").textContent = "Rinkeby test network ";
      break;
    case 42:
    case 42n:
      document.getElementById("metmaskNetworkName").textContent = "Kovan test network ";
      break;
    case 4447:
    case 4447n:
      document.getElementById("metmaskNetworkName").textContent = "Truffle Develop Network ";
      break;
    case 11155111:
    case 11155111n:
      document.getElementById("metmaskNetworkName").textContent = "Sepolia Test Network ";
      break;
    case 5777:
    case 5777n:
      document.getElementById("metmaskNetworkName").textContent = "Ganache Blockchain ";
      break;
    default :
      document.getElementById("metmaskNetworkName").textContent = "";
    }

    document.getElementById("metmaskNetworkName").textContent += "(" + networkId + ")";
  }
};

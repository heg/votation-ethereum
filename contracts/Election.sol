// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.2 <0.9.0;

contract Election {

  // adresse du compte ayant déployé le smart contract
  address public ownerAddress;

  // score des votes: publique tout le monde peut les lire
  int public aScore;
  int public bScore;
  int public counter;

  // id de l'election du moment
  int public electionId;

  // structure représentant les admins
  struct Admin {
    // le compte de l'admin
    address admin_address;
    // l'information précisant qu'il est bien admin
    bool is_admin;
  }

  // structure représentant un votant
  struct Voter {
    // le compte du votant
    address voter_address;
    // l'id de la derniere election à laquelle l'utilisateur a participé
    int lastElectionId;
    // l'information précisant qu'il a bien voté
    bool has_voted;
  }

  // tableau permettant de tester si un compte a déjà voté: interne, seul le smart contract a accès a ces données
  mapping(address => Voter) internal voters;

  // tableau permettant de tester si un compte est un admin
  mapping(address => Admin) internal admins;

  // évènement émis lors d'un vote
  event newVoteRegistered();
  event reset();

  // constructeur du contrat
  constructor () {
    // on enregiste le compte ayant déployé le smart contrat
    ownerAddress = msg.sender;

    // enregistrement du créateur en tant qu'admin
    admins[ownerAddress] = Admin({
      admin_address: ownerAddress,
      is_admin: true
    });

    // initialisation de l'id de l'election
    electionId = 1;
    // initialisation des scores
    aScore = 0;
    bScore = 0;
    counter = 0;
  }

  // Fonction publique permettant de voter pour A
  function voteForA() public onlyNonVoter{
    // on incrémente le score de A
    aScore++;
    // on enregistre le votant
    registerVoter(msg.sender);
  }

  // similaire à voteForA mais pour B
  function voteForB() public onlyNonVoter{
    bScore++;
    registerVoter(msg.sender);
  }

  // enregistrement des votes: c'est une fonction interne, elle ne peut que être appelée par le smart contrat lui même
  function registerVoter(address voter) internal {
    // enregistrement du votant
    voters[voter] = Voter({
      voter_address: voter,
      has_voted: true,
      lastElectionId: electionId
    });
    counter++;
    // on envoie un évènement précisant qu'un nouveau vote a été enregistré
    emit newVoteRegistered();
  }

  // reset the votation
  function resetElection() public onlyAdmins {
    // initialisation des scores
    aScore = 0;
    bScore = 0;
    counter = 0;

    electionId++;
    emit reset();
  }

  // add a new admin
  function addAnAdmin(address newAdmin) public onlyAdmins {
    admins[newAdmin] = Admin({
      admin_address: newAdmin,
      is_admin: true
    });
  }

    // on s'assure que l'utilisateur n'a pas voté pour le vote actuel
  modifier onlyAdmins {
      require(
        admins[msg.sender].is_admin,
        "You are not an admin"
      );
      _;
  }

    // on s'assure que l'utilisateur n'a pas voté pour le vote actuel
  modifier onlyNonVoter {
      require(
        voters[msg.sender].lastElectionId != electionId
          || !voters[msg.sender].has_voted,
        "You've already voted for this election"
      );
      _;
  }

}
